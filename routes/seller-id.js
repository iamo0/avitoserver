const openDB = require(`json-file-db`);
const path = require(`path`);


const db = openDB(path.resolve(__dirname, `../data/sellers.json`));


module.exports = (ID) => new Promise((resolve, reject) => {
  db.getSingle(ID, (err, data) => {
    console.log(err, data);

    if (err) {
      return reject({errors: [err]});
    }

    return resolve({data});
  });
});
