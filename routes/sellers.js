const openDB = require(`json-file-db`);
const path = require(`path`);


const db = openDB(path.resolve(__dirname, `../data/sellers.json`));


module.exports = () => new Promise((resolve, reject) => {
  db.get((err, data) => {
    if (err) {
      return reject({errors: [err]});
    }

    return resolve({data});
  });
});
