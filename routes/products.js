const openDB = require(`json-file-db`);
const path = require(`path`);


const db = openDB(path.resolve(__dirname, `../data/products.json`));


module.exports = () => new Promise((resolve, reject) => {
  db.get((err, data) => {
    if (err) {
      return reject({errors: [err]});
    }

    return resolve({data});
  });
});
