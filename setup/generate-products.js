const Category = require(`../lib/category`);
const {getRandomInt, getRandomItem, shuffle} = require(`../lib/utils`);


const Address = [
  [`Счетная палата городского округа Королёв`, [55.9231845, 37.8094879]],
  [`Заячий переулок`, [59.9395605, 30.3771971]],
  [`Прогулочная улица`, [51.767746, 36.174474]],
  [`Охотничья улица`, [55.795288, 37.684008]],
  [`Беговая улица`, [55.779148, 37.556777]],
  [`Стрелковая улица`, [55.945441, 37.774773]],
  [`магазин Пиф-Паф`, [55.8224199, 37.5155853]],
  [`Умирающий воин`, [54.7211838, 20.545797]],
  [`Посёлок Извоз`, [59.2033544, 29.1669852]],
  [`Боткинская больница`, [55.7814079, 37.5526154]],
  [`Воровское`, [57.2071815, 32.5979023]],
  [`Малая Рукавицкая`, [59.2000345, 37.1795223]],
  [`Буфет Отель`, [55.7457642, 37.665761]],
  [`Кражино`, [54.1241384, 26.4324806]],
  [`Конфеты`, [59.9210483, 30.3507891]],
  [`Домашняя улица`, [56.0038072, 92.8096117]],
  [`Живая вода, церковь`, [56.28839, 90.5023933]],
].map((it) => ({
  'lat': it[1][0],
  'lng': it[1][1],
}));


const ItemTitle = {
  [Category.AUTO]: {
    prefix: [
      `кроссовер`,
      `седан`,
      `универсал`,
      `хэтчбек`,
    ],
    object: [
      `Audie`,
      `BMX`,
      `Chevron`,
      `Datsund`,
    ],
  },

  [Category.CAMERAS]: {
    prefix: [
      `зеркальный фотоаппарат`,
      `цифровой фотоаппарат`,
      `экшн-камера`,
    ],
    object: [
      `Cannot`,
      `Leyka`,
      `Niko`,
    ],
  },

  [Category.IMMOVABLE]: {
    prefix: [
      `апартаменты`,
      `квартира`,
      `лофт`,
      `дом`,
    ],
    object: [
      `1 комната, 34 кв.м.`,
      `2 комнаты, 55 кв.м.`,
      `2 комнаты, 75 кв.м.`,
      `3 комнаты, 70 кв.м.`,
      `3 комнаты, 90 кв.м.`,
    ],
  },

  [Category.LAPTOPS]: {
    prefix: [
      `ноутбук`,
      `ультрабук`,
      `игровой ноутбук`,
      `неттоп`,
    ],
    object: [
      `SONI`,
      `Lenobo`,
      `Racer`,
      `DEL`,
    ],
  },
};


const CategoryToPictureRequest = {
  [Category.AUTO]: `auto,interior`,
  [Category.CAMERAS]: `camera`,
  [Category.IMMOVABLE]: `interior,apartment,luxury`,
  [Category.LAPTOPS]: `laptop`,
};


const SpecificField = {
  BODY_TYPE: 'body_type',
  CAMERA_TYPE: 'camera_type',
  GEARBOX: 'gearbox',
  LAPTOP_TYPE: 'laptop_type',
  MATRIX_RESOLUTION: 'matrix_resolution',
  PROCESSOR: 'processor',
  PROPERTY_TYPE: 'property_type',
  RAM: 'ram',
  ROOMS: 'rooms',
  SCREEN_DIAGONAL: 'screen',
  SQUARE: 'square',
  VIDEO_RESOLUTION: 'video_resolution',
  YEAR: 'year',
};


const SpecificFieldGetter = {
  [SpecificField.BODY_TYPE]: () => getRandomItem(['sedan', 'hatchback', 'suv', 'estate']),
  [SpecificField.CAMERA_TYPE]: () => getRandomItem(['slr', 'digital']),
  [SpecificField.GEARBOX]: () => getRandomItem(['automatic', 'manual']),
  [SpecificField.LAPTOP_TYPE]: () => getRandomItem(['home', 'professional', 'ultrabook']),
  [SpecificField.MATRIX_RESOLUTION]: () => getRandomInt(6, 20),
  [SpecificField.PROCESSOR]: () => getRandomItem(['i3', 'i5', 'i7']),
  [SpecificField.PROPERTY_TYPE]: () => getRandomItem(['apartment', 'flat', 'house']),
  [SpecificField.RAM]: () => getRandomItem(['4', '8', '16']),
  [SpecificField.ROOMS]: () => getRandomInt(1, 4),
  [SpecificField.SCREEN_DIAGONAL]: () => getRandomItem(['13.3', '14', '15', '17']),
  [SpecificField.SQUARE]: () => getRandomInt(24, 75),
  [SpecificField.VIDEO_RESOLUTION]: () => getRandomItem(['HD', 'Full HD', '4K', '5K']),
  [SpecificField.YEAR]: () => getRandomInt(1998, new Date().getFullYear()),
};


const CategoryToSpecificField = {
  [Category.AUTO]: [
    SpecificField.BODY_TYPE,
    SpecificField.GEARBOX,
    SpecificField.YEAR,
  ],
  [Category.CAMERAS]: [
    SpecificField.CAMERA_TYPE,    
    SpecificField.MATRIX_RESOLUTION,    
    SpecificField.VIDEO_RESOLUTION,
  ],
  [Category.IMMOVABLE]: [
    SpecificField.PROPERTY_TYPE,
    SpecificField.ROOMS,
    SpecificField.SQUARE,
  ],
  [Category.LAPTOPS]: [
    SpecificField.LAPTOP_TYPE,
    SpecificField.PROCESSOR,
    SpecificField.RAM,
    SpecificField.SCREEN_DIAGONAL,
  ],
};


const addBasicInfo = (baseObj) => Object.assign({}, baseObj, {
  address: getRandomItem(Address),
  category: getRandomItem([...Object.values(Category)]),
  title: ``,
});


const addPrice = (baseObj) => Math.random() > 0.05 ? Object.assign({}, baseObj, {
  price: getRandomInt(2000, 2000000),
}) : baseObj;


const addTitle = (baseObj) => Object.assign({}, baseObj, {
  title: [
    getRandomItem(ItemTitle[baseObj.category].prefix),
    getRandomItem(ItemTitle[baseObj.category].object)
  ].join(` `)
});


const addPictures = (baseObj) => Object.assign({}, baseObj, {
  pictures: new Array(getRandomInt(3, 7))
      .fill(``)
      .map(() => `//loremflickr.com/400/400/${
        CategoryToPictureRequest[baseObj.category]}?random=${getRandomInt(1, 100)}`)
});


const addSeller = (sellers, baseObj) => Object.assign({}, baseObj, {
  relationships: {
    seller: getRandomItem(
      sellers.filter((it) => it.category === baseObj.category)
    ).id
  },
});


const addSpecific = (baseObj) => CategoryToSpecificField[baseObj.category]
    .slice(getRandomInt(0, 1))
    .reduce((acc, it) => Object.assign({}, acc, {
      [it]: SpecificFieldGetter[it]()
    }), Object.assign({}, baseObj));


module.exports = (amount, sellers) => new Array(amount).fill({})
    .map(addBasicInfo)
    .map(addPrice)
    .map(addTitle)
    .map(addPictures)
    .map(addSpecific)
    .map(addSeller.bind(null, sellers));
