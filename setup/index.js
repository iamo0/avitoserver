const fs = require(`fs`);
const generateSellers = require(`./generate-sellers`);
const generateProducts = require(`./generate-products`);
const openDB = require(`json-file-db`);
const path = require(`path`);


const PRODUCTS_PATH = path.resolve(__dirname, `../data/products.json`);
const SELLERS_PATH = path.resolve(__dirname, `../data/sellers.json`);


const addID = (list) => list.map((it, i) => Object.assign({}, it, {
  id: `${i}`,
}));


const writeSellers = (cb) => {
  const sellers = addID(generateSellers());
  fs.writeFile(SELLERS_PATH, JSON.stringify(sellers), `utf-8`, cb);
};


const writeProducts = (sellers, cb) => {
  console.log(sellers);
  const products = addID(generateProducts(100, sellers));
  console.log(products);
  fs.writeFile(PRODUCTS_PATH, JSON.stringify(products), `utf-8`, cb);
};


const done = () => {
  console.log(`All data is written`);
};


const init = () => {
  const sellersDB = openDB(path.resolve(__dirname, `../data/sellers.json`));

  writeSellers(() => {
    sellersDB.get((err, sellers) => {
      if (err) {
        console.error(err);
        return;
      }

      writeProducts(sellers, done);
    });
  });
};


init();
