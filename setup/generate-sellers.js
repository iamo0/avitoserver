const Category = require(`../lib/category`);
const commonCategory = Category.commonCategory;
const {getRandom, getRandomInt, getRandomItem} = require(`../lib/utils`);


const SellerName = {
  [Category.AUTO]: [
    `Ауди-центр Кунцево`,
    `БМВ-центр Север через Северо-Запад`,
    `Вольво-центр Юг`,
    `Geely-Москва`,
    `Datsun-Подмосковье`,
  ],

  [Category.CAMERAS]: [
    `DNF`,
    `Ульмарт`,
    `Фотострана+`,
    `Ф-Видео`,
  ],

  [Category.IMMOVABLE]: [
    `Агентство недвижимости «Купчино»`,
    `Агентство недвижимости «Озерки»`,
    `Агентство недвижимости «Левый берег»`,
    `Строительная компания «Строй-Траст-Инвест`,
    `Строительная компания «Трансмашстрой»`,
    `Строительная компания «СК-188»`,
  ],

  [Category.LAPTOPS]: [
    `DMS`,
    `Re:Stor`,
    `Н-Видео`,
    `Эльдорадость`,
  ],

  [commonCategory]: [
    `Антон Чехов`,
    `Аркадий Аверченко`,
    `Лев Толстой`,
    `Максим Горький`,
    `Сергей Довлатов`,
    `Фёдор Достоевский`,
  ],
};


const getSeller = (category, baseObj) => Object.assign({}, baseObj, {
  category,
  isCompany: Math.random() > 0.5,
  name: getRandomItem([].concat(
      SellerName[category],
      SellerName[commonCategory].slice(0, getRandomInt(
          0,
          SellerName[commonCategory].length
      ))
  )),
  rating: parseFloat(getRandom(2, 5).toFixed(1)),
});


module.exports = () => Object.keys(SellerName)
    .filter((it) => it !== commonCategory)
    .reduce((acc, categoryName) => acc.concat(
        new Array(SellerName[categoryName].length)
            .fill({})
            .map(getSeller.bind(null, categoryName)))
        , []);
