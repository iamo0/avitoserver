const express = require(`express`);
const products = require(`./routes/products`);
const productID = require(`./routes/product-id`);
const sellers = require(`./routes/sellers`);
const sellerID = require(`./routes/seller-id`);
const {URL} = require(`url`);


const app = express();


const PORT = 5551;


const ApiURL = {
  PRODUCTS: `/products`,
  SELLERS: `/sellers`,
};


app.use((req, res, next) => {
  res.header(`Access-Control-Allow-Origin`, `*`);
  res.header(`Access-Control-Allow-Headers`, `Origin, X-Requested-With, Content-Type, Accept`);
  next();
});


app.get(`/`, (req, res) => {
  const base = `${req.protocol}://${req.hostname}`;
  
  res.status(200).json({
    'links': {
      'products': new URL(ApiURL.PRODUCTS, base).toString(),
      'product': new URL(`${ApiURL.PRODUCTS}/:product_id`, base).toString(),
      'sellers': new URL(ApiURL.SELLERS, base).toString(),
      'seller': new URL(`${ApiURL.SELLERS}/:seller_id`, base).toString(),
    },
  });
});


app.get(`${ApiURL.PRODUCTS}`, (req, res) => {
  products()
      .then((data) => res.status(200).json(data))
      .catch(() => res.sendStatus(500));
});


app.get(`${ApiURL.PRODUCTS}/:id`, (req, res) => {
  productID(req.params.id)
      .then((data) => res.status(200).json(data))
      .catch(() => res.sendStatus(500));
});


app.get(`${ApiURL.SELLERS}`, (req, res) => {
  sellers()
      .then((data) => res.status(200).json(data))
      .catch(() => res.sendStatus(500));
});


app.get(`${ApiURL.SELLERS}/:id`, (req, res) => {
  sellerID(req.params.id)
      .then((data) => res.status(200).json(data))
      .catch(() => res.sendStatus(500));
});


app.listen(
    PORT,
    () => console.log(`Started server at ${PORT}`)
);
