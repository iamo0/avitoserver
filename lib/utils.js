const getRandom = (min, max) => Math.random() * (max - min) + min;


const getRandomInt = (min, max) => parseInt(getRandom(min, max), 10);


const getRandomItem = (arr) => arr[getRandomInt(0, arr.length - 1)];


module.exports = {
  getRandom,
  getRandomInt,
  getRandomItem,
};
