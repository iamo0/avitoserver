module.exports = {
  AUTO: `auto`,
  CAMERAS: `cameras`,
  IMMOVABLE: `immovable`,
  LAPTOPS: `laptops`,
  commonCategory: `common`,
};
